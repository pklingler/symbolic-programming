; cs378.clj                Gordon S. Novak Jr.       28 Aug 18

; (load-file "/u/novak/cs378/patm.clj")
;                    tst.xyz.core
(ns user
  (:use clojure.test )
  (:require [clojure.string :as str] ))
;            [clojure.math.numeric-tower :as math]

(def lstnum '(85 90 85 96 86 83 86 88 84 82 66 71 98 76 75 98 88 85))

(defn varp [v]
  (and (symbol? v)
       (str/starts-with? (name v) "?") ) )

(defn assocl [key lst]
  (if (empty? lst)
      nil
      (if (= (first (first lst)) key)
          (first lst)
          (assocl key (rest lst)) ) ) )

(defn equal [x y]
  (if (and (list? x) (not (empty? x)))
      (and (list? y) (not (empty? y))
           (equal (first x) (first y))
           (equal (rest x) (rest y)))
      (= x y) ))

(defn null? [x] (or (= x nil) (= x '()) ) )

(defn matchb [pat inp bindings]
  (if (and (seq? bindings) (not (empty? bindings)))
       (if (and (seq? pat) (not (empty? pat)))
	   (and (seq? inp) (not (empty? inp))
		(matchb (rest pat) 
			(rest inp)
			(matchb (first pat)
				(first inp) bindings)))
	   (if (varp pat)
	       (if (not (empty? (assocl pat bindings)))
		   (and (equal inp (second (assocl pat bindings)))
		        bindings)
                   (cons (list pat inp) bindings))
	       (and (= pat inp) bindings)))) )

(defn match [pat inp]  (matchb pat inp '((t t))))

(defn subst [new old form]
  (if (and (seq? form) (not (empty? form)))
      (cons (subst new old (first form))
            (subst new old (rest form)))
      (if (= form old)
          new
          form) ) ) 

(defn sublis [alist form]
  (if (and (seq? form) (not (empty? form)))
      (cons (sublis alist (first form))
            (sublis alist (rest form)))
      (let [binding (assocl form alist)]
          (if binding
              (second binding)
              form) ) ) )

(defn copy-tree [form]
  (if (and (seq? form) (not (empty? form)))
      (cons (copy-tree (first form))
            (copy-tree (rest form)))
      form) )

(defn transform [pattern-pair input]
  (let [bindings (match (first pattern-pair) input)]
    (if bindings
        (sublis bindings (second pattern-pair)) ) ))

(defn op [e] (first e))
(defn lhs [e] (second e))
(defn rhs [e] (first (rest (rest e))))

(defn append [x y]
  (if (empty? x)
      y
      (cons (first x)
            (append (rest x) y)) ) )

(defn member [item lst]
  (if (empty? lst)
      nil
      (if (= item (first lst))
          lst
          (member item (rest lst)) ) ) )

(defn intersection [x y]
  (if (empty? x)
      '()
      (if (member (first x) y)
          (cons (first x)
                (intersection (rest x) y))
          (intersection (rest x) y) ) ) )

(defn trrevb [lst answer]
  (if (empty? lst)
      answer
      (trrevb (rest lst)
              (cons (first lst) answer)) ) )

(defn trrev [lst] (trrevb lst '()))

(defn lengthb [lst answer]
  (if (empty? lst)         ; test for base case
      answer               ; answer for base case
      (lengthb (rest lst)  ; recursive call
               (+ answer 1)) ) )   ; update answer

(defn length [lst]
  (lengthb lst 0))         ; init extra variable

(defn square [x] (* x x))

(defn abs [x] (if (< x 0) (- x) x) )

; sqrt by Newton-Raphson iteration, recursive functional style
(defn sqrtstep [x estimate]
  (* 0.5 (+ estimate (/ x estimate))) )

(defn sqrtfn [x estimate n]
  (if (< (abs (- (* estimate estimate) x)) 1.0e-8)
      (sqrtstep x estimate)   ; once more for good measure
      (if (> n 8)
          estimate
          (sqrtfn x (sqrtstep x estimate) (+ n 1)) ) ) )

(defn sqrt [x] (if (>= x 0) (sqrtfn x 1.0 0)))

(defn expt [x n]
  (if (= n 0)
      1
      (if (> n 0)
          (* x (expt x (- n 1)))
          (/ 1 (expt x (- n))) ) ) )

(defn sum [lst] 
  (if (empty? lst)
      0
      (+ (first lst) (sum (rest lst)) ) ) )

(defn sumr [lst]
  (reduce + lst))

(defn sumtr 
  ([lst] (sumtr lst 0))
  ([lst acc]
    (if (empty? lst)
      acc
      (sumtr (+ acc (first lst)) (rest lst) ) ) ) )

(defn sumsq [lst]
  (if (empty? lst)
    0
    (+ (expt (first lst)) (sumsq lst)) ) )

(defn sumsqr [lst]
  (reduce + (map (fn [x] (expt x 2)) lst)) )

(defn sumsqtr 
  ([lst] (sumtr lst 0))
  ([lst acc]
    (if (empty? lst)
      acc
      (sumsqtr (+ acc (expt 2 (first lst))) (rest lst) ) ) ) )

(defn mean [lst]
  (if (empty? lst)
    0
    (/ (sumr lst) (length lst)) ) )

(defn stdev [lst]
  (if (empty? lst)
    0
    (- (/ (sumsqtr lst) (length lst)) (expt (mean lst) 2) ) ) )

(defn union [lsta lstb]
  (if (empty? lsta)
      lstb
      (if (not (member (first lsta) lstb))
          (union (rest lsta) (cons (first lsta) lstb) )
          (union (rest lsta) lstb) ) ) )

(defn set-difference [lsta lstb]
  (if (empty? lsta)
      '()
      (if (not (member (first lsta) lstb))
          (cons (first lsta)
                (set-difference (rest lsta) lstb))
          (set-difference (rest lsta) lstb) ) ) )

(defn next-binom-row [row]
  (if (= row '(1))
    row
    (cons (+ (first row) (second row)) (next-binom-row (rest row))) ) )

(defn binomial [n]
  (if (= n 0)
    `(1)
    (cons 1 (next-binom-row (binomial (- n 1)))) ) )

(defn next-binom-row [row]
  (if (= row '(1))
    row
    (cons (+ (first row) (second row)) (next-binom-row (rest row))) ) )

(defn binomial [n]
  (if (= n 0)
    `(1)
    (cons 1 (next-binom-row (binomial (- n 1)))) ) )

(defn get-vars [exp]
  (if (symbol? exp)
    (cons exp '())
    ('()) ) )

(defn vars [expr]
  (if (empty? expr)
    '()
    (if (list? (first expr))
      (union (vars (first expr)) (vars (rest expr)))
      (union (get-vars (first expr)) (vars (rest expr))) ) ) )

(defn occurs [item tree]
  (if (list? tree)
    (if (empty? tree) 
      false
      (or (occurs item (first tree)) (occurs item (rest tree))) )
    (= item tree) ) )

(defn maxbt [tree]
  (if (number? tree)
    tree
    (if (or (not (list? tree)) (empty? tree))
        -999999
        (max (maxbt (first tree)) (maxbt (rest tree))) ) ) )

(defn myeval [tree]
  (if (list? tree)
      ((resolve (first tree)) (myeval (second tree)) (myeval (last tree)))
      tree ) )

(defn myevalb [tree bindings]
  (if (list? tree)
      (if (= (length tree) 3)
        ((resolve (first tree)) (myevalb (second tree) bindings) (myevalb (last tree) bindings))
        ((resolve (first tree)) (myevalb (second tree) bindings)) )
      (if (number? tree)
        tree
        (second (assocl tree bindings)) ) ) )
  
(defn preced [exp]
  (if (= exp '=) 1 (if (member exp '(+ -)) 5 6)) )

(defn basic [o] (member o '(- + * / =)))

(defn basic-exp [o l r surr-preced]
  (if (nil? r)
    (str "(" o l ")")
    (if (>= surr-preced (preced o)) 
      (str "(" l o r ")")
      (str l o r) ) ) )

(defn complex-exp [o r] (str "Math." o "(" r ")"))

(defn tojava
  ([tree]
    (str (tojava tree 0) ";") )
  ([tree surr-preced]
    (if (list? tree)
      (let [o (first tree) len (length tree)]
        (let [l (tojava (second tree) (preced o))
              r (if (= len 3) 
                    (tojava (last tree) (preced o))
                    nil)]
          (if (basic o)
            (basic-exp o l r surr-preced) 
            (complex-exp o l) ) ) )
      tree ) ) )
